const express = require('express');
const router = express.Router();

router.post('/',(req, res, next) => {
  console.log(`Req params ${req.params}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.get('/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.put('/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.delete('/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

module.exports = router;
